import { TestBed, inject } from '@angular/core/testing';

import { TodoService } from './todo.service';
import { ApiService } from './api.service';

describe('TodoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodoService, { provide: ApiService }]
    });
  });

  it('should be created', inject([TodoService], (service: TodoService) => {
    expect(service).toBeTruthy();
  }));
});
