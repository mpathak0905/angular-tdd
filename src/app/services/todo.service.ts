import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ToDo } from '../model/todo.model';

@Injectable()
export class TodoService {
  private urls: any = {
    get: 'http://localhost:3000/',
    post: 'http://localhost:3000/'
  };
  constructor(private apiService: ApiService) {}

  add(todo: ToDo) {
    return this.apiService.add(this.urls.post, todo);
  }

  get() {
    return this.apiService.get(this.urls.get);
  }
}
