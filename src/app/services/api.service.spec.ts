import { TestBed } from '@angular/core/testing';
import { ApiService } from './api.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { ToDo } from '../model/todo.model';
import { of } from 'rxjs';

describe('ApiService', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;
  let urls: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });
    service = TestBed.get(ApiService);
    httpMock = TestBed.get(HttpTestingController);
    urls = {
      get: 'http://localhost:3000/',
      post: 'http://localhost:3000/'
    };
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return array of todos', () => {
    service.get(urls.get).subscribe((data: ToDo[]) => {
      expect(data).toBeDefined();
      expect(data.length).toBeGreaterThan(0);
    });

    const req = httpMock.expectOne(urls.get, 'Api call to get todos');
    expect(req.request.method).toBe('GET');

    req.flush([
      {
        title: 'Dummy To Do'
      }
    ]);

    httpMock.verify();
  });

  it('should create a todo and return 201', () => {
    service.add(urls.post, { title: 'new todo' }).subscribe((data: boolean) => {
      expect(data).toBeDefined();
      expect(data).toBeTruthy();
    });

    const req = httpMock.expectOne(urls.post, 'Api call to add todo');
    expect(req.request.method).toBe('POST');
    req.flush(of(true), { status: 201, statusText: 'Added' });

    httpMock.verify();
  });

  it('should fail to create a todo when invalid request body and return 400', () => {
    service.add(urls.post, {}).subscribe(
      (data: boolean) => {
      },
      (err: any) => {
        expect(err).toBeDefined();
      }
    );

    const req = httpMock.expectOne(urls.post, 'Api call to add todo');
    expect(req.request.method).toBe('POST');
    req.flush(of(false), { status: 400, statusText: 'Invalid Request' });

    httpMock.verify();
  });
});
