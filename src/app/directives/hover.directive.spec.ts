import { HoverDirective } from './hover.directive';
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
@Component({
  template: `
    <h1 appHover>Dummy Component</h1>
  `
})
class DummyComponent {}

describe('HoverDirective', () => {
  let fixture: ComponentFixture<DummyComponent>;
  let h1El: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DummyComponent, HoverDirective]
    });
    fixture = TestBed.createComponent(DummyComponent);
    h1El = fixture.debugElement.query(By.css('h1'));
  });

  it('should create an instance', () => {
    const directive = new HoverDirective();
    expect(directive).toBeTruthy();
  });

  it('should change background color on hover and revert when out', () => {
    h1El.triggerEventHandler('mouseover', null);
    fixture.detectChanges();
    expect(h1El.nativeElement.style.backgroundColor).toBe('yellow');
    h1El.triggerEventHandler('mouseout', null);
    fixture.detectChanges();
    expect(h1El.nativeElement.style.backgroundColor).toBe('inherit');
  });
});
