import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
export class HoverDirective {
  @HostBinding('style.background-color') bgColor: string;
  @HostListener('mouseover') onMouseOver() {
    this.bgColor = 'yellow';
  }
  @HostListener('mouseout') onmouseout() {
    this.bgColor = 'inherit';
  }
}
