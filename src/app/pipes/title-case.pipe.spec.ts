import { TitleCasePipe } from './title-case.pipe';

describe('TitleCasePipe', () => {
  const pipe = new TitleCasePipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('should convert text into Title Case', () => {
    let value = 'to do title';
    let expectedValue = 'To Do Title';
    let pipedValue = pipe.transform(value);
    expect(pipedValue).toBe(expectedValue);
  });
});
