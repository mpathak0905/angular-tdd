import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleCase'
})
export class TitleCasePipe implements PipeTransform {
  transform(value: string): any {
    if (!value || value.length == 0) {
      return value;
    } else {
      return value
        .split(' ')
        .map((part) => {
          return part.substr(0, 1).toLocaleUpperCase() + part.substr(1);
        })
        .join(' ');
    }
  }
}
