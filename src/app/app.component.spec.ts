import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TodoComponent } from './components/todo/todo.component';
import { AddTodoComponent } from './components/todo/addtodo/addtodo.component';
import { FormsModule } from '@angular/forms';
import { ApiService } from './services/api.service';
import { TodoService } from './services/todo.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HoverDirective } from './directives/hover.directive';
import { TitleCasePipe } from './pipes/title-case.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientTestingModule],
      declarations: [
        AppComponent,
        TodoComponent,
        AddTodoComponent,
        HoverDirective,
        TitleCasePipe
      ],
      providers: [ApiService, TodoService]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
