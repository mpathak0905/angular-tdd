import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoComponent } from './todo.component';
import { AddTodoComponent } from './addtodo/addtodo.component';
import { FormsModule } from '@angular/forms';
import { TodoService } from 'src/app/services/todo.service';
import { ApiService } from 'src/app/services/api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { By } from '@angular/platform-browser';
import { HoverDirective } from 'src/app/directives/hover.directive';
import { TitleCasePipe } from 'src/app/pipes/title-case.pipe';

describe('TodoComponent', () => {
  let component: TodoComponent;
  let fixture: ComponentFixture<TodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientTestingModule],
      declarations: [
        TodoComponent,
        AddTodoComponent,
        HoverDirective,
        TitleCasePipe
      ],
      providers: [TodoService, ApiService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('todos should be defined', () => {
    expect(component.todos).toBeDefined();
  });

  it('after init, getToDos function should be called', () => {
    fixture.whenStable().then(() => {
      expect(component.getToDos).toHaveBeenCalled();
    });
  });

  it('after add button is clicked, addToDo should have been called', () => {
    spyOn(component, 'addToDo');
    let child = fixture.debugElement.query(By.directive(AddTodoComponent));
    let childComponent = child.componentInstance;
    childComponent.onAdd.emit('New To Do');
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.addToDo).toHaveBeenCalledWith('New To Do');
    });
  });
});
