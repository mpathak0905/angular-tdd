import { Component, OnInit } from '@angular/core';
import { ToDo } from '../../model/todo.model';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  todos: ToDo[];
  constructor(private toDoService: TodoService) {
    this.todos = [];
  }

  ngOnInit() {
    this.getToDos();
  }

  getToDos() {
    this.toDoService.get().subscribe(
      (data: ToDo[]) => {
        this.todos = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  addToDo(title: string) {
    this.toDoService.add({ title }).subscribe(
      (success: boolean) => {
        if (success) {
          console.log('Added successfully');
          this.getToDos();
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
