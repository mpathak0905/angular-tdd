import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTodoComponent } from './addtodo.component';
import { FormsModule } from '@angular/forms';

describe('AddtodoComponent', () => {
  let component: AddTodoComponent;
  let fixture: ComponentFixture<AddTodoComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [AddTodoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have empty title', () => {
    fixture.whenStable().then(() => {
      expect(component.title).toBeDefined();
      expect(component.title).toBe('');
    });
  });

  it('should have empty title', () => {
    component.title = 'New todo';
    spyOn(component.onAdd, 'emit');
    
    let button = fixture.nativeElement.querySelector('button');
    button.dispatchEvent(new Event('click'));

    fixture.detectChanges();
    expect(component.onAdd.emit).toHaveBeenCalledWith('New todo');
    expect(component.title).toBe('');
  });
});
