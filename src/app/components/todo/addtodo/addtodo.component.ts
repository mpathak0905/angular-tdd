import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-addtodo',
  templateUrl: './addtodo.component.html',
  styleUrls: ['./addtodo.component.css']
})
export class AddTodoComponent implements OnInit {
  title: string = '';
  @Output() onAdd: EventEmitter<string> = new EventEmitter<string>();
  constructor() {}

  ngOnInit() {}

  addToDo() {
    this.onAdd.emit(this.title);
    this.title = '';
  }
}
