import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TodoComponent } from './components/todo/todo.component';
import { AddTodoComponent } from './components/todo/addtodo/addtodo.component';
import { ApiService } from './services/api.service';
import { TodoService } from './services/todo.service';
import { HoverDirective } from './directives/hover.directive';
import { TitleCasePipe } from './pipes/title-case.pipe';

@NgModule({
  declarations: [AppComponent, TodoComponent, AddTodoComponent, HoverDirective, TitleCasePipe],
  imports: [BrowserModule, HttpClientModule, FormsModule],
  providers: [ApiService, TodoService],
  bootstrap: [AppComponent]
})
export class AppModule {}
