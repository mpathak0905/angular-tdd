const express = require('express');
const bodyParser = require('body-parser');

const port = 3000 || process.env.PORT;
const app = express();

var todos = [];

const addToDo = (todo) => {
  todos.push(todo);
};

app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});
app.get('/', (req, res) => {
  res.send(todos);
});

app.post('/', (req, res) => {
  let todo = req.body;
  if (todo && todo.title) {
    addToDo(todo);
    res.status(201).send(true);
  } else {
    res.status(400).send(false);
  }
});

app.listen(port, () => {
  console.log(`Listening on: http://localhost:${port}`);
});
